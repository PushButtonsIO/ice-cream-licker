// setup
(function(){
  
  paper.install(window);
  paper.setup('myCanvas');
  // set canvas size
  view.setViewSize(600, 600);
  
  // new rectangle [offset, size]
  var path = new Path.Rectangle([100,100], [400, 400]);
  path.fillColor = '#e9e9ff';
  // show points
  path.selected = true;
  
  // toppie
  var toppie = function(amount){
    path.segments[1].handleOut.y = amount;
    path.segments[1].handleOut.x = 200;
  }
  // bottie
  var bottie = function(amount){
    path.segments[3].handleOut.y = amount;
    path.segments[3].handleOut.x = -200;
  }
  
  var updatePath = function(){
    if (!scrollSpeed) return
    toppie(scrollSpeed);
    bottie(scrollSpeed);
  }
  
  view.onFrame = function() {
    updatePath()
  };

//Calculating The Scrolling Speed
  var prev, next, scrollSpeed = 0;
  var i = 0;
  var invertScroll = true;
  
  var $container = $(window); 
  
  var slice = function(val, max) {
    if (val > 0 && val > max) {
      return Math.min(val, max);
    }
    if (val < 0 && val < max) {
      return Math.max(val, -max);
    }
    return val;
  };
  
  $(window).scroll(function(){
    if (i % 4 === 0) {
      var direction = invertScroll ? -1 : 1;
      next = $container.scrollTop();
      scrollSpeed = direction * slice(1.2 * (next - prev), 200)
      prev = next;
      //console.log(scrollSpeed);
    }
    i++;
  })

})();